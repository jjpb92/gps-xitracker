import { set, toggle } from '@/utils/vuex'

export default {
  setDrawer: set('drawer'),
  setImage: set('image'),
  setColor: set('color'),
  setMenu: set('menu'),
  setToken: set('token'),
  toggleDrawer: toggle('drawer')
}
