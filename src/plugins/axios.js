import Vue from 'vue'
// Lib imports
import axios from 'axios'
import router from '@/router'

axios.defaults.baseURL = 'http://186.84.249.148:9090/api/';


if(localStorage.getItem('AUTH_TOKEN')){
  axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('AUTH_TOKEN')}`;
}else {
  router.push({name:'login'})
}

axios.interceptors.response.use(function (response) {
  // Any status code that lie within the range of 2xx cause this function to trigger
  // Do something with response data
  return response;
}, function (error,d) {
  // Any status codes that falls outside the range of 2xx cause this function to trigger
  // Do something with response error
  console.log(error,d)
  return Promise.reject(error);
});


Vue.prototype.$http = axios
