/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
  {
    path: '/',
    // Relative to /src/views
    view: 'Dashboard'
  },
  {
    path: '/login',
    name: 'Login',
    view: 'Login'
  },
  {
    path: '/ubicacion',
    name: 'Ubicacion',
    view: 'Ubicacion/Dashboard'
  },
  {
    path: '/tipo-vehiculo',
    name: 'Tipo Vehiculo',
    view: 'TipoVehiculo/Listar'
  },
  {
    path: '/marca-vehiculo',
    name: 'Marca Vehiculo',
    view: 'MarcaVehiculo/Listar'
  },
  {
    path: '/tipo-documento',
    name: 'Tipo Documento',
    view: 'TipoDocumento/Listar'
  },
  {
    path: '/color',
    name: 'Color Vehiculo',
    view: 'Color/Listar'
  },
  {
    path: '/user-profile',
    name: 'User Profile',
    view: 'UserProfile'
  },
  {
    path: '/table-list',
    name: 'Table List',
    view: 'TableList'
  },
  {
    path: '/typography',
    view: 'Typography'
  },
  {
    path: '/icons',
    view: 'Icons'
  },
  {
    path: '/maps',
    view: 'Maps'
  },
  {
    path: '/notifications',
    view: 'Notifications'
  },
  {
    path: '/upgrade',
    name: 'Upgrade to PRO',
    view: 'Upgrade'
  }
]
