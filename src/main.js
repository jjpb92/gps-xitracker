import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import vuetify from './plugins/vuetify'
import { sync } from 'vuex-router-sync'

import './plugins'
import './components'

sync(store, router)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
 ...App
})
