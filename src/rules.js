export const required = (v)=>!!v || 'Campo Obligatorio';

export const email = (v)=>/.+@.+/.test(v) || 'Email Invalido';
